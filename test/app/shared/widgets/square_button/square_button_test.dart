import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:pandapaysubmission/app/shared/widgets/square_button/square_button.dart';

main() {
  testWidgets('SquareButton has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(
      SquareButton(),
    ));
    final textFinder = find.text('SquareButton');
    expect(textFinder, findsOneWidget);
  });
}
