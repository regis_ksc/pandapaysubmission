import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:pandapaysubmission/app/shared/widgets/register_with_button/register_with_button.dart';

main() {
  testWidgets('RegisterWithButton has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(RegisterWithButton()));
    final textFinder = find.text('RegisterWithButton');
    expect(textFinder, findsOneWidget);
  });
}
