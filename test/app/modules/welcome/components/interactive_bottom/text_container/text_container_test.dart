import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:pandapaysubmission/app/modules/welcome/components/interactive_bottom/text_container/text_container.dart';

main() {
  testWidgets('TextContainer has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(
      TextContainer(),
    ));
    final textFinder = find.text('TextContainer');
    expect(textFinder, findsOneWidget);
  });
}
