import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:pandapaysubmission/app/modules/welcome/components/interactive_bottom/button_container/button_container.dart';

main() {
  testWidgets('ButtonContainer has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(
      ButtonContainer(),
    ));
    final textFinder = find.text('ButtonContainer');
    expect(textFinder, findsOneWidget);
  });
}
