import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';

import 'package:pandapaysubmission/app/modules/welcome/components/interactive_bottom/interactive_bottom_widget.dart';

main() {
  testWidgets('InteractiveBottomWidget has message',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(
      InteractiveBottomWidget(),
    ));
    final textFinder = find.text('InteractiveBottom');
    expect(textFinder, findsOneWidget);
  });
}
