import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:pandapaysubmission/app/modules/welcome/components/interactive_bottom/footer_container/footer_container.dart';

main() {
  testWidgets('FooterContainer has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(
      FooterContainer(),
    ));
    final textFinder = find.text('FooterContainer');
    expect(textFinder, findsOneWidget);
  });
}
