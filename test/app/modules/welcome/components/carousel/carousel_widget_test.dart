import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';

import 'package:pandapaysubmission/app/modules/welcome/components/carousel/carousel_widget.dart';

main() {
  testWidgets('CarouselWidget has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(
      CarouselWidget(),
    ));
    final textFinder = find.text('Carousel');
    expect(textFinder, findsOneWidget);
  });
}
