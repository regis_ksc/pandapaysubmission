import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:pandapaysubmission/app/modules/login/login_fields/login_fields.dart';

main() {
  testWidgets('LoginFields has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(LoginFields()));
    final textFinder = find.text('LoginFields');
    expect(textFinder, findsOneWidget);
  });
}
