import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';

import 'package:pandapaysubmission/app/modules/login/login_fields/login/login_widget.dart';

main() {
  testWidgets('LoginWidget has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(LoginWidget()));
    final textFinder = find.text('Login');
    expect(textFinder, findsOneWidget);
  });
}
