import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';

import 'package:pandapaysubmission/app/modules/login/login_fields/password/password_widget.dart';

main() {
  testWidgets('PasswordWidget has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(PasswordWidget()));
    final textFinder = find.text('Password');
    expect(textFinder, findsOneWidget);
  });
}
