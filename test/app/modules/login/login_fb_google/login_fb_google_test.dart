import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:pandapaysubmission/app/modules/login/login_fb_google/login_fb_google.dart';

main() {
  testWidgets('LoginFbGoogle has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(LoginFbGoogle()));
    final textFinder = find.text('LoginFbGoogle');
    expect(textFinder, findsOneWidget);
  });
}
