import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:pandapaysubmission/app/modules/login/login_button_and_forgot_pass/login_button_and_forgot_pass.dart';

main() {
  testWidgets('LoginButtonAndForgotPass has message',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(LoginButtonAndForgotPass()));
    final textFinder = find.text('LoginButtonAndForgotPass');
    expect(textFinder, findsOneWidget);
  });
}
