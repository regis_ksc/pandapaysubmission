import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:pandapaysubmission/app/modules/register/components/register_button_row/register_button_row.dart';

main() {
  testWidgets('RegisterButtonRow has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(
      RegisterButtonRow(),
    ));
    final textFinder = find.text('RegisterButtonRow');
    expect(textFinder, findsOneWidget);
  });
}
