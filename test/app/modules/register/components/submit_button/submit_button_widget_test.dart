import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';

import 'package:pandapaysubmission/app/modules/register/components/submit_button/submit_button_widget.dart';

main() {
  testWidgets('SubmitButtonWidget has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(SubmitButtonWidget()));
    final textFinder = find.text('SubmitButton');
    expect(textFinder, findsOneWidget);
  });
}
