import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:pandapaysubmission/app/modules/register/components/register_regular_fields/register_name_row/register_name_row.dart';

main() {
  testWidgets('RegisterNameRow has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(
      RegisterNameRow(),
    ));
    final textFinder = find.text('RegisterNameRow');
    expect(textFinder, findsOneWidget);
  });
}
