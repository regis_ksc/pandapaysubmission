import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:pandapaysubmission/app/modules/register/components/register_regular_fields/register_doc_row/register_doc_row.dart';

main() {
  testWidgets('RegisterDocRow has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(
      RegisterDocRow(),
    ));
    final textFinder = find.text('RegisterDocRow');
    expect(textFinder, findsOneWidget);
  });
}
