import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:pandapaysubmission/app/modules/register/components/register_regular_fields/register_phone_row/register_phone_row.dart';

main() {
  testWidgets('RegisterPhoneRow has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(
      RegisterPhoneRow(),
    ));
    final textFinder = find.text('RegisterPhoneRow');
    expect(textFinder, findsOneWidget);
  });
}
