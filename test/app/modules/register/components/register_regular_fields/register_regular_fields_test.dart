import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:pandapaysubmission/app/modules/register/components/register_regular_fields/register_regular_fields.dart';

main() {
  testWidgets('RegisterRegularFields has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(
      RegisterRegularFields(),
    ));
    final textFinder = find.text('RegisterRegularFields');
    expect(textFinder, findsOneWidget);
  });
}
