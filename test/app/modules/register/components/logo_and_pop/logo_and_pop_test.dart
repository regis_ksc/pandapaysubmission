import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:pandapaysubmission/app/modules/register/components/logo_and_pop/logo_and_pop.dart';

main() {
  testWidgets('LogoAndPop has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(
      LogoAndPop(),
    ));
    final textFinder = find.text('LogoAndPop');
    expect(textFinder, findsOneWidget);
  });
}
