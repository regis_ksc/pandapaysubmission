import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';

import 'package:pandapaysubmission/app/modules/register/components/register_email_fields/visibility_false/visibility_false_widget.dart';

main() {
  testWidgets('VisibilityFalseWidget has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(VisibilityFalseWidget()));
    final textFinder = find.text('VisibilityFalse');
    expect(textFinder, findsOneWidget);
  });
}
