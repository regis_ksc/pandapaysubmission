import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:pandapaysubmission/app/modules/register/components/register_email_fields/register_confirmpassword_row/register_confirmpassword_row.dart';

main() {
  testWidgets('RegisterConfirmPasswordRow has message',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(RegisterConfirmPasswordRow()));
    final textFinder = find.text('RegisterConfirmpasswordRow');
    expect(textFinder, findsOneWidget);
  });
}
