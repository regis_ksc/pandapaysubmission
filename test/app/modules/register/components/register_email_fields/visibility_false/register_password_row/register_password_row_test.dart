import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:pandapaysubmission/app/modules/register/components/register_email_fields/visibility_false/register_password_row/register_password_row.dart';

main() {
  testWidgets('RegisterPasswordRow has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(RegisterPasswordRow()));
    final textFinder = find.text('RegisterPasswordRow');
    expect(textFinder, findsOneWidget);
  });
}
