import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:pandapaysubmission/app/modules/register/components/register_email_fields/visibility_false/register_email_row/register_email_row.dart';

main() {
  testWidgets('RegisterEmailRow has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(RegisterEmailRow()));
    final textFinder = find.text('RegisterEmailRow');
    expect(textFinder, findsOneWidget);
  });
}
