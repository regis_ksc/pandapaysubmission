import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';

import 'package:pandapaysubmission/app/modules/register/components/register_email_fields/visibility_true/visibility_true_widget.dart';

main() {
  testWidgets('VisibilityTrueWidget has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(VisibilityTrueWidget()));
    final textFinder = find.text('VisibilityTrue');
    expect(textFinder, findsOneWidget);
  });
}
