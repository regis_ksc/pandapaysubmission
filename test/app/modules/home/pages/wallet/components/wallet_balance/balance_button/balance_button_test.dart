import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:pandapaysubmission/app/modules/home/pages/wallet/components/wallet_balance/balance_button/balance_button.dart';

main() {
  testWidgets('BalanceButton has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(BalanceButton()));
    final textFinder = find.text('BalanceButton');
    expect(textFinder, findsOneWidget);
  });
}
