import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:pandapaysubmission/app/modules/home/pages/wallet/components/wallet_balance/wallet_balance.dart';

main() {
  testWidgets('WalletBalance has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(WalletBalance()));
    final textFinder = find.text('WalletBalance');
    expect(textFinder, findsOneWidget);
  });
}
