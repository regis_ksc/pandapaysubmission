import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:pandapaysubmission/app/modules/home/pages/wallet/components/wallet_cards/wallet_cards.dart';

main() {
  testWidgets('WalletCards has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(WalletCards()));
    final textFinder = find.text('WalletCards');
    expect(textFinder, findsOneWidget);
  });
}
