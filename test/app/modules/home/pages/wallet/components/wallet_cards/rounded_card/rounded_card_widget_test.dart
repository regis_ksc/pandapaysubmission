import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';

import 'package:pandapaysubmission/app/modules/home/pages/wallet/components/wallet_cards/rounded_card/rounded_card_widget.dart';

main() {
  testWidgets('RoundedCardWidget has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(RoundedCardWidget()));
    final textFinder = find.text('RoundedCard');
    expect(textFinder, findsOneWidget);
  });
}
