import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:pandapaysubmission/app/modules/home/pages/wallet/components/wallet_footer/avatar/avatar.dart';

main() {
  testWidgets('Avatar has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(Avatar()));
    final textFinder = find.text('Avatar');
    expect(textFinder, findsOneWidget);
  });
}
