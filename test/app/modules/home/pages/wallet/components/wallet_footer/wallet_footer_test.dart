import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:pandapaysubmission/app/modules/home/pages/wallet/components/wallet_footer/wallet_footer.dart';

main() {
  testWidgets('WalletFooter has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(WalletFooter()));
    final textFinder = find.text('WalletFooter');
    expect(textFinder, findsOneWidget);
  });
}
