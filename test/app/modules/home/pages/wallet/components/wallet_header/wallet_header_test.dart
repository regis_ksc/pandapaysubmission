import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:pandapaysubmission/app/modules/home/pages/wallet/components/wallet_header/wallet_header.dart';

main() {
  testWidgets('WalletHeader has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(WalletHeader()));
    final textFinder = find.text('WalletHeader');
    expect(textFinder, findsOneWidget);
  });
}
