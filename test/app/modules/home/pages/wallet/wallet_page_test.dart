import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';

import 'package:pandapaysubmission/app/modules/home/pages/wallet/wallet_page.dart';

main() {
  testWidgets('WalletPage has title', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(WalletPage(title: 'Wallet')));
    final titleFinder = find.text('Wallet');
    expect(titleFinder, findsOneWidget);
  });
}
