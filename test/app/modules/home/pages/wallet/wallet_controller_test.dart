import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:pandapaysubmission/app/modules/home/pages/wallet/wallet_controller.dart';
import 'package:pandapaysubmission/app/modules/home/home_module.dart';

void main() {
  initModule(HomeModule());
  WalletController wallet;

  setUp(() {
    wallet = HomeModule.to.get<WalletController>();
  });

  group('WalletController Test', () {
    test("First Test", () {
      expect(wallet, isInstanceOf<WalletController>());
    });

    test("Set Value", () {
      expect(wallet.value, equals(0));
      wallet.increment();
      expect(wallet.value, equals(1));
    });
  });
}
