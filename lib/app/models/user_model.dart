import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class UserModel {
  UserModel({this.id, this.document, this.email, this.name, this.phone});

  String id;
  String name;
  String document;
  String phone;
  String email;

  FirebaseAuth _auth = FirebaseAuth.instance;

  FirebaseUser firebaseUser;

  Map<String, dynamic> userInfo = Map();

  void register(
      {@required Map<String, dynamic> userInfo,
      @required String password,
      @required VoidCallback onSuccess,
      @required VoidCallback onFailure}) {
    _auth
        .createUserWithEmailAndPassword(
            email: userInfo['email'], password: password)
        .then((user) async {
      // SUCCESS
      firebaseUser = user.user;
      await _saveUser(userInfo);
    }).catchError((e) {
      // ERROR
      onFailure();
    });
  }

  void login() {}

  Future<Null> _saveUser(Map<String, dynamic> userInfo) async {
    this.userInfo = userInfo;
    await Firestore.instance
        .collection("users")
        .document(firebaseUser.uid)
        .setData(userInfo);
  }
}
