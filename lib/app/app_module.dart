import 'package:pandapaysubmission/app/app_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter/material.dart';
import 'package:pandapaysubmission/app/app_widget.dart';
import 'package:pandapaysubmission/app/modules/login/login_module.dart';
import 'package:pandapaysubmission/app/modules/register/register_module.dart';
import 'package:pandapaysubmission/app/modules/welcome/welcome_controller.dart';
import 'package:pandapaysubmission/app/modules/welcome/welcome_module.dart';
import 'package:pandapaysubmission/app/repositories/user_repository.dart';

import 'modules/home/home_module.dart';
import 'modules/splashscreen/splashscreen_module.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind((i) => UserRepository()),
        Bind(
          (i) => AppController(),
        ),
        Bind(
          (i) => WelcomeController(),
        ),
      ];

  @override
  List<Router> get routers => [
        Router('/', module: SplashscreenModule()),
        Router('/welcome/',
            module: WelcomeModule(), transition: TransitionType.fadeIn),
        Router('/home/',
            module: HomeModule(), transition: TransitionType.fadeIn),
        Router('/register/',
            module: RegisterModule(), transition: TransitionType.fadeIn),
        Router('/login/',
            module: LoginModule(), transition: TransitionType.fadeIn),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
