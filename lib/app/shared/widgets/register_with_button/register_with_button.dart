import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';

class RegisterWithButton extends StatelessWidget {
  final Widget icon;
  final String text;
  final Color bgColor;
  final Color borderColor;
  final Color textColor;
  final Function onTap;

  const RegisterWithButton(
      {Key key,
      this.icon,
      this.text,
      this.bgColor,
      this.borderColor,
      this.textColor,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      padding: EdgeInsets.zero,
      onPressed: onTap,
      child: Container(
        height: 5.454049 * SizeConfig.heightMultiplier,
        width: double.infinity,
        decoration: BoxDecoration(
          color: bgColor,
          borderRadius:
              BorderRadius.circular(7.088176 * SizeConfig.widthMultiplier),
          border: Border.all(color: borderColor, width: 1),
        ),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 25,
              child: Container(
                child: Container(
                  margin: EdgeInsets.symmetric(
                      vertical: 0.681756 * SizeConfig.heightMultiplier,
                      horizontal: 2.83527 * SizeConfig.widthMultiplier),
                  height: double.infinity,
                  width: double.infinity,
                  child: icon,
                ),
              ),
            ),
            Expanded(
              flex: 50,
              child: Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  text,
                  style: titleFont.copyWith(
                      color: textColor,
                      fontSize: SizeConfig.heightMultiplier * 1.636214889),
                ),
              ),
            ),
            Expanded(
              flex: 25,
              child: Container(),
            ),
          ],
        ),
      ),
    );
  }
}
