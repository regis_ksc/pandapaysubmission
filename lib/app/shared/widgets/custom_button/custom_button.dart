import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final Color splashColor;
  final Color color;
  final Color borderColor;
  final Text text;
  final Function onTap;
  final double radius;
  final double height;
  final double width;
  final EdgeInsets padding;

  const CustomButton(
      {Key key,
      this.color,
      this.text,
      this.borderColor,
      this.onTap,
      this.radius,
      this.height,
      this.width,
      this.padding,
      this.splashColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
      splashColor: splashColor ?? null,
      onPressed: onTap,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(radius),
      ),
      child: Container(
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(radius),
          border: Border.all(color: borderColor ?? Colors.black),
        ),
        padding: padding,
        height: height,
        width: width,
        child: Center(
          child: FittedBox(child: text),
        ),
      ),
    );
  }
}
