import 'package:flutter/material.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';

TextStyle buttonContentFont = TextStyle(
  fontFamily: 'OpenSans',
  color: white95,
  fontSize: 1.9089 * SizeConfig.heightMultiplier,
  decoration: TextDecoration.none,
  letterSpacing: SizeConfig.widthMultiplier * 0.141763,
);
TextStyle titleFont = TextStyle(
  fontFamily: 'OpenSans',
  fontSize: 2.1816 * SizeConfig.heightMultiplier,
  decoration: TextDecoration.none,
  letterSpacing: SizeConfig.widthMultiplier * 0.141763,
);
TextStyle registerFormFieldFont = TextStyle(
  fontFamily: 'OpenSans',
  fontSize: 1.6362 * SizeConfig.heightMultiplier,
  fontWeight: FontWeight.bold,
  color: Color(0xBF000000),
  decoration: TextDecoration.none,
  letterSpacing: 0.1417635 * SizeConfig.widthMultiplier,
);
const TextStyle varelaFont = TextStyle(
  fontFamily: 'Varela',
  decoration: TextDecoration.none,
);
const TextStyle robotoFont = TextStyle(
  fontFamily: 'Roboto',
  decoration: TextDecoration.none,
);

const Color blackButtonColor = Color(0xE6000000);
const Color facebookBlue = Color(0xFF475786);
const Color white95 = Color(0xF5FFFFFF);
const Color lightGreenOpaque = Color(0xFF19FA25);
const Color lightRedOpaque = Color(0xFFFE4947);
