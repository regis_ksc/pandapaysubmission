import 'package:animator/animator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'splashscreen_controller.dart';

class SplashscreenPage extends StatefulWidget {
  final String title;
  const SplashscreenPage({Key key, this.title = "Splashscreen"})
      : super(key: key);

  @override
  _SplashscreenPageState createState() => _SplashscreenPageState();
}

class _SplashscreenPageState
    extends ModularState<SplashscreenPage, SplashscreenController>
    with SingleTickerProviderStateMixin {
  //use 'controller' variable to access controller

  @override
  void initState() {
    super.initState();
    pushScreen();
  }

  Future<void> pushScreen() async {
    return Future.delayed(
      const Duration(milliseconds: 2500),
      () => Modular.to.pushReplacementNamed('/welcome/'),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          color: Colors.black,
        ),
        Animator(
          tween: Tween<double>(begin: 0, end: 1),
          curve: Curves.easeInQuart,
          duration: Duration(milliseconds: 1200),
          builder: (animation) => Center(
            child: Opacity(
              opacity: animation.value,
              child: Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                  image: AssetImage('assets/images/splash.png'),
                )),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
