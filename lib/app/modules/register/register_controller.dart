import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

part 'register_controller.g.dart';

class RegisterController = _RegisterControllerBase with _$RegisterController;

abstract class _RegisterControllerBase with Store {
  @observable
  bool isCpf = true;
  @observable
  bool isMail = false;
  @observable
  bool phoneValidated = false;

  @action
  void validatePhone() {
    phoneValidated = true;
  }

  @action
  void changeIsCpf() {
    isCpf = !isCpf;
  }

  @action
  void changeIsMail() {
    isMail = !isMail;
  }

  final formKey = GlobalKey<FormState>();

  @observable
  String name;
  @action
  setName(String value) => name = value;
  @observable
  String document;
  @action
  setDocument(String value) => document = value;
  @observable
  String phone;
  @action
  setPhone(String value) => phone = value;
  @observable
  String email;
  @action
  setEmail(String value) => email = value;
  @observable
  String password;
  @action
  setPassword(String value) => password = value;
  @observable
  String confPassword;
  @action
  setConfPassword(String value) => confPassword = value;

  @action
  resetData() {
    phoneValidated = false;
    isCpf = true;
    isMail = false;
    document = null;
    phone = null;
    email = null;
    password = null;
    confPassword = null;
  }
}
