import 'package:flutter/material.dart';
import 'package:pandapaysubmission/app/modules/register/components/register_email_fields/visibility_false/register_confirm_password_row/register_confirm_password_row.dart';
import 'package:pandapaysubmission/app/modules/register/components/register_email_fields/visibility_false/register_email_row/register_email_row.dart';
import 'package:pandapaysubmission/app/modules/register/components/register_email_fields/visibility_false/register_password_row/register_password_row.dart';

class VisibilityFalseWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      child: RegisterEmailRow(),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      child: RegisterPasswordRow(),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      child: RegisterConfirmPasswordRow(),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
