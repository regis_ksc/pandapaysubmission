import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pandapaysubmission/app/modules/register/register_controller.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';

class RegisterPasswordRow extends StatelessWidget {
  var controller = Modular.get<RegisterController>();
  var passwordController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
          flex: 30,
          child: Container(
            child: Observer(builder: (_) {
              return Text(
                "Senha",
                style: titleFont.copyWith(
                    fontSize: 1.9089 * SizeConfig.heightMultiplier,
                    color: Colors.grey[800]),
              );
            }),
          ),
        ),
        Expanded(
          flex: 70,
          child: Material(
            color: Colors.transparent,
            child: Container(
              padding: EdgeInsets.symmetric(
                vertical: 1.877229 * SizeConfig.widthMultiplier,
              ),
              child: Observer(builder: (_) {
                return TextFormField(
                  obscureText: true,
                  style: registerFormFieldFont.copyWith(
                      letterSpacing: 0.56705 * SizeConfig.widthMultiplier),
                  textCapitalization: TextCapitalization.words,
                  cursorWidth: 0.1,
                  controller: passwordController,
                  onChanged: (String text) {
                    if (passwordController.text.length < 6) {
                      controller.setPassword(null);
                    } else {
                      controller.setPassword(passwordController.text);
                    }
                  },
                  decoration: InputDecoration(
                    hintText: "mínimo 6 caracteres",
                    hasFloatingPlaceholder: true,
                    hintStyle: TextStyle(
                        color: Colors.grey[400],
                        fontFamily: 'OpenSans',
                        fontSize: 1.6362 * SizeConfig.heightMultiplier,
                        letterSpacing: 0.1417635 * SizeConfig.widthMultiplier),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          width: 0.187722920968 * SizeConfig.widthMultiplier,
                          color: Colors.grey[500]),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          width:
                              1.5 * 0.187722920968 * SizeConfig.widthMultiplier,
                          color: Colors.blueGrey[800]),
                    ),
                    errorBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          width: 0.187722920968 * SizeConfig.widthMultiplier,
                          color: Colors.blueGrey[500]),
                    ),
                    contentPadding: EdgeInsets.symmetric(
                      vertical: SizeConfig.heightMultiplier * 1.363512,
                    ),
                  ),
                );
              }),
            ),
          ),
        ),
      ],
    );
  }
}
