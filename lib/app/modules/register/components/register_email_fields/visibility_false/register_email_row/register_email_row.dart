import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';

import '../../../../register_controller.dart';

class RegisterEmailRow extends StatelessWidget {
  var controller = Modular.get<RegisterController>();
  var emailController = new TextEditingController();

  final InputDecoration decoration;

  RegisterEmailRow({Key key, this.decoration}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
          flex: 30,
          child: Container(
            child: Observer(builder: (_) {
              return Text(
                "E-mail",
                style: titleFont.copyWith(
                    fontSize: 1.9089 * SizeConfig.heightMultiplier,
                    color: Colors.grey[800]),
              );
            }),
          ),
        ),
        Expanded(
          flex: 70,
          child: Material(
            color: Colors.transparent,
            child: Container(
              padding: EdgeInsets.symmetric(
                vertical: 1.877229 * SizeConfig.widthMultiplier,
              ),
              child: Observer(builder: (_) {
                return TextFormField(
                  style: registerFormFieldFont.copyWith(
                      letterSpacing: 0.1417635 * SizeConfig.widthMultiplier),
                  textCapitalization: TextCapitalization.words,
                  cursorWidth: 0.1,
                  controller: emailController,
                  onChanged: (String text) {
                    Pattern pattern =
                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                    RegExp regex = new RegExp(pattern);
                    if (emailController.text.isEmpty ||
                        !regex.hasMatch(emailController.text))
                      controller.setEmail(null);
                    else
                      controller.setEmail(emailController.text);
                  },
                  decoration: InputDecoration(
                    hintText: "joao@email.com.br",
                    hasFloatingPlaceholder: true,
                    hintStyle: TextStyle(
                        color: Colors.grey[400],
                        fontFamily: 'OpenSans',
                        fontSize: 1.6362 * SizeConfig.heightMultiplier),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          width: 0.187722920968 * SizeConfig.widthMultiplier,
                          color: Colors.grey[500]),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          width:
                              1.5 * 0.187722920968 * SizeConfig.widthMultiplier,
                          color: Colors.blueGrey[800]),
                    ),
                    errorBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          width: 0.187722920968 * SizeConfig.widthMultiplier,
                          color: Colors.blueGrey[500]),
                    ),
                    contentPadding: EdgeInsets.symmetric(
                      vertical: 1.363512 * SizeConfig.heightMultiplier,
                    ),
                  ),
                );
              }),
            ),
          ),
        ),
      ],
    );
  }
}
