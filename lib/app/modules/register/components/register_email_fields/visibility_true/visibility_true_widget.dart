import 'package:flutter/material.dart';
import 'package:pandapaysubmission/app/shared/widgets/register_with_button/register_with_button.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';

class VisibilityTrueWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.fromLTRB(
                      SizeConfig.widthMultiplier * 11.341083, // LEFT
                      SizeConfig.heightMultiplier * 0.6817562, // TOP
                      SizeConfig.widthMultiplier * 11.341083, //RIGHT
                      0 // BOTTOM
                      ),
                  child: RegisterWithButton(
                    textColor: Colors.white70,
                    bgColor: facebookBlue,
                    borderColor: facebookBlue,
                    text: "Facebook",
                    icon: Container(
                      margin: EdgeInsets.symmetric(
                          vertical: SizeConfig.heightMultiplier * 0.272702,
                          horizontal: SizeConfig.widthMultiplier * 0.567054),
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          fit: BoxFit.fitHeight,
                          image: AssetImage(
                            'assets/images/register/facebook.png',
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: EdgeInsets.fromLTRB(
                      SizeConfig.widthMultiplier * 11.341083, // LEFT
                      SizeConfig.heightMultiplier * 0.6817562, // TOP
                      SizeConfig.widthMultiplier * 11.341083, //RIGHT
                      0 // BOTTOM
                      ),
                  child: RegisterWithButton(
                    textColor: Colors.black87,
                    borderColor: Colors.black12,
                    bgColor: Colors.white70,
                    text: "Google",
                    icon: Container(
                      margin: EdgeInsets.symmetric(
                          vertical: SizeConfig.heightMultiplier * 0.272702,
                          horizontal: SizeConfig.widthMultiplier * 0.567054),
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          fit: BoxFit.fitHeight,
                          image: AssetImage(
                            'assets/images/register/google.png',
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: Container(),
        ),
      ],
    );
  }
}
