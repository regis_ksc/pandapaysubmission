import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pandapaysubmission/app/models/user_model.dart';
import 'package:pandapaysubmission/app/modules/register/register_controller.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';
import 'package:pandapaysubmission/app/shared/widgets/custom_button/custom_button.dart';

class SubmitButtonWidget extends StatelessWidget {
  var controller = Modular.get<RegisterController>();
  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius:
              BorderRadius.circular(SizeConfig.widthMultiplier * 7.088176)),
      child: Container(
        child: CustomButton(
          color: blackButtonColor,
          borderColor: blackButtonColor,
          height: SizeConfig.heightMultiplier * 5.45404,
          width: double.infinity,
          text: Text(
            "Cadastrar",
            style: buttonContentFont,
          ),
          radius: SizeConfig.widthMultiplier * 7.088176,
          onTap: () {
            if (controller.email != null &&
                controller.document != null &&
                controller.name != null &&
                controller.password != null &&
                (controller.password == controller.confPassword) &&
                controller.phone != null &&
                controller.phoneValidated == true) {
              Map<String, dynamic> regUser = {
                "name": controller.name,
                "document": controller.document,
                "email": controller.email,
                "phone": controller.phone,
              };

              UserModel model = UserModel();
              model.register(
                  userInfo: regUser,
                  password: controller.password,
                  onSuccess: () {},
                  onFailure: () {});
              Modular.to.pushReplacementNamed('/home/');
            } else {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      content: FittedBox(
                        child: Container(
                          height: SizeConfig.heightMultiplier * 50,
                          width: SizeConfig.widthMultiplier * 70,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Visibility(
                                visible: controller.name == null,
                                child: Text(
                                  "Digite um nome válido.",
                                ),
                              ),
                              Visibility(
                                visible: controller.document == null,
                                child: Text(
                                  controller.isCpf
                                      ? "Digite um CPF válido."
                                      : "Digite um CNPJ válido.",
                                ),
                              ),
                              Visibility(
                                visible: controller.phone == null,
                                child: Text(
                                  "Digite um telefone válido.",
                                ),
                              ),
                              Visibility(
                                visible: controller.phoneValidated == false,
                                child: Text(
                                  "Valide seu telefone.",
                                ),
                              ),
                              Visibility(
                                visible: controller.email == null,
                                child: Text(
                                  "Digite um email válido.",
                                ),
                              ),
                              Visibility(
                                visible: controller.password == null,
                                child: Text(
                                  "Digite uma senha maior que 6 caracteres.",
                                ),
                              ),
                              Visibility(
                                visible: !(controller.password ==
                                            controller.confPassword) &&
                                        controller.password == null ||
                                    controller.confPassword == null,
                                child: Text(
                                  "As senhas digitadas não são iguais.",
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      actions: [
                        RaisedButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          color: Colors.black,
                          child: Text("OK",
                              style: buttonContentFont.copyWith(
                                fontSize: SizeConfig.heightMultiplier * 1.8,
                                color: white95,
                              )),
                        ),
                      ],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                    );
                  });
            }
          },
          padding: EdgeInsets.zero,
        ),
      ),
    );
  }
}
