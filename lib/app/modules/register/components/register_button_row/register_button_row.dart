import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pandapaysubmission/app/modules/register/register_controller.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';
import 'package:pandapaysubmission/app/shared/widgets/custom_button/custom_button.dart';

class RegisterButtonRow extends StatefulWidget {
  @override
  _RegisterButtonRowState createState() => _RegisterButtonRowState();
}

class _RegisterButtonRowState
    extends ModularState<RegisterButtonRow, RegisterController> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          flex: 43,
          child: Observer(builder: (_) {
            return CustomButton(
              padding: EdgeInsets.symmetric(
                  horizontal: 2.835270 * SizeConfig.widthMultiplier,
                  vertical: 1.3635124079 * SizeConfig.heightMultiplier),
              color: controller.isCpf ? blackButtonColor : Colors.transparent,
              borderColor:
                  controller.isCpf ? blackButtonColor : Colors.grey[800],
              radius: 7.0881769 * SizeConfig.widthMultiplier,
              text: Text(
                "PARA MIM",
                style: buttonContentFont.copyWith(
                    color: !controller.isCpf ? Colors.blueGrey[700] : white95),
              ),
              height: 5.454049 * SizeConfig.heightMultiplier,
              onTap: () {
                !controller.isCpf ? controller.changeIsCpf() : null;
              },
            );
          }),
        ),
        Expanded(
          flex: 14,
          child: Container(),
        ),
        Expanded(
          flex: 43,
          child: Observer(builder: (_) {
            return CustomButton(
              color: controller.isCpf ? Colors.transparent : blackButtonColor,
              borderColor:
                  controller.isCpf ? Colors.grey[800] : blackButtonColor,
              radius: 7.0881769 * SizeConfig.widthMultiplier,
              padding: EdgeInsets.symmetric(
                  horizontal: 1.417635 * SizeConfig.widthMultiplier,
                  vertical: 2.83527 / 2 * SizeConfig.heightMultiplier),
              text: Text(
                "PARA MEU NEGOCIO",
                style: buttonContentFont.copyWith(
                    color: controller.isCpf ? Colors.blueGrey[700] : white95),
              ),
              height: SizeConfig.heightMultiplier * 5.454049,
              onTap: () {
                controller.isCpf ? controller.changeIsCpf() : null;
              },
            );
          }),
        ),
      ],
    );
  }
}
