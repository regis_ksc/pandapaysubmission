import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';

class LogoAndPop extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
          horizontal: 4.2529061 * SizeConfig.widthMultiplier,
          vertical: 1.36351240796 * SizeConfig.heightMultiplier),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 10,
            child: Center(
              child: GestureDetector(
                onTap: () {
                  Modular.to.pop();
                },
                child: Icon(
                  Ionicons.md_arrow_round_back,
                  size: SizeConfig.widthMultiplier * 8.505812,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 50,
            child: Center(
              child: Container(
                margin: EdgeInsets.only(
                    left: SizeConfig.widthMultiplier * 1.417635384),
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image:
                          AssetImage('assets/images/register/pandapayhb.png'),
                      fit: BoxFit.contain),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 40,
            child: Container(),
          )
        ],
      ),
    );
  }
}
