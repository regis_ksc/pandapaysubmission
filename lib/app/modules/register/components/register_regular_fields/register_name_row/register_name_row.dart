import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:pandapaysubmission/app/modules/register/register_controller.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';

class RegisterNameRow extends StatelessWidget {
  var controller = Modular.get<RegisterController>();
  var nameController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
          flex: 35,
          child: Container(
            child: Observer(builder: (_) {
              return Text(
                controller.isCpf ? "Nome Completo" : "Razão Social",
                style: titleFont.copyWith(
                    fontSize: 1.908917 * SizeConfig.heightMultiplier,
                    color: Colors.grey[800]),
              );
            }),
          ),
        ),
        Expanded(
          flex: 65,
          child: Material(
            color: Colors.transparent,
            child: Container(
              padding: EdgeInsets.symmetric(
                vertical: SizeConfig.heightMultiplier * 1.363512,
              ),
              child: Observer(builder: (_) {
                return TextFormField(
                    style: registerFormFieldFont,
                    textCapitalization: TextCapitalization.words,
                    cursorWidth: 0.1,
                    controller: nameController,
                    onChanged: (String text) {
                      if (nameController.text.length < 2)
                        controller.setName(null);
                      else
                        controller.setName(nameController.text);
                    },
                    decoration: InputDecoration(
                      hintText: controller.isCpf
                          ? "Como está no seu RG"
                          : "Nome do seu negócio",
                      hasFloatingPlaceholder: true,
                      hintStyle: TextStyle(
                          color: Colors.grey[500],
                          fontFamily: 'OpenSans',
                          fontSize: 12,
                          letterSpacing:
                              0.1417635 * SizeConfig.widthMultiplier),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                            width: 0.28352707 * SizeConfig.widthMultiplier,
                            color: Colors.grey[500]),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                            width:
                                1.5 * 0.28352707 * SizeConfig.widthMultiplier,
                            color: Colors.blueGrey[800]),
                      ),
                      errorBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                            width: 0.28352707 * SizeConfig.widthMultiplier,
                            color: Colors.blueGrey[500]),
                      ),
                      contentPadding: EdgeInsets.symmetric(
                        vertical: 1.3635124 * SizeConfig.heightMultiplier,
                      ),
                    ));
              }),
            ),
          ),
        ),
      ],
    );
  }
}
