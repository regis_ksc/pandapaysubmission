import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pandapaysubmission/app/modules/register/register_controller.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';
import 'package:pandapaysubmission/app/shared/widgets/custom_button/custom_button.dart';

class RegisterPhoneRow extends StatelessWidget {
  var phoneController = new MaskedTextController(mask: '(00) 00000 - 0000');
  var controller = Modular.get<RegisterController>();
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          flex: 70,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 30,
                child: Text(
                  "Celular",
                  style: titleFont.copyWith(
                      fontSize: 1.908917 * SizeConfig.heightMultiplier,
                      color: Colors.grey[800]),
                ),
              ),
              Expanded(
                flex: 70,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 25,
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 1.6 * SizeConfig.heightMultiplier,
                            horizontal: 3.4023249 * SizeConfig.widthMultiplier),
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white38,
                              borderRadius: BorderRadius.circular(
                                  SizeConfig.widthMultiplier * 2.835270)),
                          child: FittedBox(
                            child: Text(
                              "+55",
                              style: titleFont.copyWith(
                                  fontSize: 1 * SizeConfig.heightMultiplier,
                                  color: Colors.grey[800]),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 75,
                      child: Observer(builder: (_) {
                        return Material(
                          color: Colors.transparent,
                          child: Padding(
                            padding: EdgeInsets.only(
                                bottom: SizeConfig.heightMultiplier * 1.22716,
                                right: SizeConfig.widthMultiplier * 2.83527),
                            child: TextFormField(
                              style: registerFormFieldFont.copyWith(
                                  letterSpacing:
                                      0.4252905 * SizeConfig.widthMultiplier),
                              controller: phoneController,
                              cursorWidth: 0.1,
                              keyboardType: TextInputType.number,
                              onChanged: (String text) {
                                if (phoneController.text.length < 17)
                                  controller.setPhone(null);
                                else
                                  controller.setPhone(phoneController.text);
                              },
                              decoration: InputDecoration(
                                hintText: "(00) 00000 - 0000",
                                hasFloatingPlaceholder: true,
                                hintStyle: TextStyle(
                                    color: Colors.grey[500],
                                    fontFamily: 'OpenSans',
                                    fontSize: 12,
                                    letterSpacing:
                                        0.1417635 * SizeConfig.widthMultiplier),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      width: 0.28352707 *
                                          SizeConfig.widthMultiplier,
                                      color: Colors.grey[500]),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      width: 1.5 *
                                          0.28352707 *
                                          SizeConfig.widthMultiplier,
                                      color: Colors.blueGrey[800]),
                                ),
                                errorBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      width: 0.28352707 *
                                          SizeConfig.widthMultiplier,
                                      color: Colors.blueGrey[500]),
                                ),
                                contentPadding: EdgeInsets.symmetric(
                                  vertical:
                                      1.3635124 * SizeConfig.heightMultiplier,
                                ),
                              ),
                            ),
                          ),
                        );
                      }),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        Expanded(
          flex: 30,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                    bottom: SizeConfig.heightMultiplier * 0.2727024),
                child: CustomButton(
                  splashColor: Colors.transparent,
                  width: 75,
                  padding: EdgeInsets.symmetric(
                      vertical: SizeConfig.heightMultiplier * 1.090809),
                  color: Colors.grey[500],
                  borderColor: Colors.grey[500],
                  height: 35,
                  radius: 25,
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text("Validando seu numero..."),
                            content:
                                Text("Clique em OK para liberar o cadastro."),
                            actions: [
                              RaisedButton(
                                onPressed: () {
                                  controller.validatePhone();
                                  Navigator.of(context).pop();
                                },
                                color: Colors.black,
                                child: Text("OK",
                                    style: buttonContentFont.copyWith(
                                      fontSize:
                                          SizeConfig.heightMultiplier * 1.8,
                                      color: white95,
                                    )),
                              ),
                            ],
                          );
                        });
                  },
                  text: Text(
                    "Verificar",
                    style: buttonContentFont,
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
