import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pandapaysubmission/app/modules/register/components/register_regular_fields/register_doc_row/register_doc_row.dart';
import 'package:pandapaysubmission/app/modules/register/components/register_regular_fields/register_name_row/register_name_row.dart';
import 'package:pandapaysubmission/app/modules/register/components/register_regular_fields/register_phone_row/register_phone_row.dart';
import 'package:pandapaysubmission/app/modules/register/register_controller.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';
import 'package:pandapaysubmission/app/shared/widgets/custom_button/custom_button.dart';

class RegisterRegularFields extends StatefulWidget {
  @override
  _RegisterRegularFieldsState createState() => _RegisterRegularFieldsState();
}

class _RegisterRegularFieldsState
    extends ModularState<RegisterRegularFields, RegisterController> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: RegisterNameRow(),
        ),
        Expanded(
          child: RegisterDocRow(),
        ),
        Expanded(
          child: RegisterPhoneRow(),
        ),
      ],
    );
  }
}
