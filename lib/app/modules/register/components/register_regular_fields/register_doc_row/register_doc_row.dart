import 'package:cpf_cnpj_validator/cnpj_validator.dart';
import 'package:cpf_cnpj_validator/cpf_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:pandapaysubmission/app/modules/register/register_controller.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';

class RegisterDocRow extends StatelessWidget {
  var cpfController = new MaskedTextController(mask: '000.000.000-00');
  var cnpjController = new MaskedTextController(mask: '00.000.000/0000-00');

  @override
  Widget build(BuildContext context) {
    var controller = Modular.get<RegisterController>();
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
          flex: 35,
          child: Container(
            child: Observer(builder: (_) {
              return Text(
                controller.isCpf ? "CPF" : "CNPJ",
                style: titleFont.copyWith(
                    fontSize: 1.908917 * SizeConfig.heightMultiplier,
                    color: Colors.grey[800]),
              );
            }),
          ),
        ),
        Expanded(
          flex: 65,
          child: Material(
            color: Colors.transparent,
            child: Container(
              padding: EdgeInsets.symmetric(
                vertical: SizeConfig.heightMultiplier * 1.363512,
              ),
              child: Observer(builder: (_) {
                return TextFormField(
                  style: registerFormFieldFont,
                  controller: controller.isCpf ? cpfController : cnpjController,
                  cursorWidth: 0.1,
                  keyboardType: TextInputType.number,
                  onChanged: (String text) {
                    print(
                        'variavel : cpfController -> ${cpfController.text} e ${cpfController.text.length}');
                    print(
                        'variavel : cnpjController -> ${cnpjController.text} e ${cnpjController.text.length}');
                    print('variavel : text -> $text e ${text.length}');
                    if (controller.isCpf) {
                      if (cpfController.text.length != 11 + 3 ||
                          !CPFValidator.isValid(cpfController.text))
                        controller.setDocument(null);
                      else
                        controller.setDocument(cpfController.text);
                    } else {
                      if (cnpjController.text.length != 14 + 4 ||
                          !CNPJValidator.isValid(cnpjController.text))
                        controller.setDocument(null);
                      else
                        controller.setDocument(cnpjController.text);
                    }
                  },
                  decoration: InputDecoration(
                    hintText: controller.isCpf
                        ? "000.000.000-00"
                        : "00.000.000/0000-00",
                    hasFloatingPlaceholder: true,
                    hintStyle: TextStyle(
                        color: Colors.grey[500],
                        fontFamily: 'OpenSans',
                        fontSize: 12,
                        letterSpacing: 0.1417635 * SizeConfig.widthMultiplier),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          width: 0.28352707 * SizeConfig.widthMultiplier,
                          color: Colors.grey[500]),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          width: 1.5 * 0.28352707 * SizeConfig.widthMultiplier,
                          color: Colors.blueGrey[800]),
                    ),
                    errorBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          width: 0.28352707 * SizeConfig.widthMultiplier,
                          color: Colors.blueGrey[500]),
                    ),
                    contentPadding: EdgeInsets.symmetric(
                      vertical: 1.3635124 * SizeConfig.heightMultiplier,
                    ),
                  ),
                );
              }),
            ),
          ),
        ),
      ],
    );
  }
}
