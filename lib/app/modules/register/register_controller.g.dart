// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$RegisterController on _RegisterControllerBase, Store {
  final _$isCpfAtom = Atom(name: '_RegisterControllerBase.isCpf');

  @override
  bool get isCpf {
    _$isCpfAtom.context.enforceReadPolicy(_$isCpfAtom);
    _$isCpfAtom.reportObserved();
    return super.isCpf;
  }

  @override
  set isCpf(bool value) {
    _$isCpfAtom.context.conditionallyRunInAction(() {
      super.isCpf = value;
      _$isCpfAtom.reportChanged();
    }, _$isCpfAtom, name: '${_$isCpfAtom.name}_set');
  }

  final _$isMailAtom = Atom(name: '_RegisterControllerBase.isMail');

  @override
  bool get isMail {
    _$isMailAtom.context.enforceReadPolicy(_$isMailAtom);
    _$isMailAtom.reportObserved();
    return super.isMail;
  }

  @override
  set isMail(bool value) {
    _$isMailAtom.context.conditionallyRunInAction(() {
      super.isMail = value;
      _$isMailAtom.reportChanged();
    }, _$isMailAtom, name: '${_$isMailAtom.name}_set');
  }

  final _$phoneValidatedAtom =
      Atom(name: '_RegisterControllerBase.phoneValidated');

  @override
  bool get phoneValidated {
    _$phoneValidatedAtom.context.enforceReadPolicy(_$phoneValidatedAtom);
    _$phoneValidatedAtom.reportObserved();
    return super.phoneValidated;
  }

  @override
  set phoneValidated(bool value) {
    _$phoneValidatedAtom.context.conditionallyRunInAction(() {
      super.phoneValidated = value;
      _$phoneValidatedAtom.reportChanged();
    }, _$phoneValidatedAtom, name: '${_$phoneValidatedAtom.name}_set');
  }

  final _$nameAtom = Atom(name: '_RegisterControllerBase.name');

  @override
  String get name {
    _$nameAtom.context.enforceReadPolicy(_$nameAtom);
    _$nameAtom.reportObserved();
    return super.name;
  }

  @override
  set name(String value) {
    _$nameAtom.context.conditionallyRunInAction(() {
      super.name = value;
      _$nameAtom.reportChanged();
    }, _$nameAtom, name: '${_$nameAtom.name}_set');
  }

  final _$documentAtom = Atom(name: '_RegisterControllerBase.document');

  @override
  String get document {
    _$documentAtom.context.enforceReadPolicy(_$documentAtom);
    _$documentAtom.reportObserved();
    return super.document;
  }

  @override
  set document(String value) {
    _$documentAtom.context.conditionallyRunInAction(() {
      super.document = value;
      _$documentAtom.reportChanged();
    }, _$documentAtom, name: '${_$documentAtom.name}_set');
  }

  final _$phoneAtom = Atom(name: '_RegisterControllerBase.phone');

  @override
  String get phone {
    _$phoneAtom.context.enforceReadPolicy(_$phoneAtom);
    _$phoneAtom.reportObserved();
    return super.phone;
  }

  @override
  set phone(String value) {
    _$phoneAtom.context.conditionallyRunInAction(() {
      super.phone = value;
      _$phoneAtom.reportChanged();
    }, _$phoneAtom, name: '${_$phoneAtom.name}_set');
  }

  final _$emailAtom = Atom(name: '_RegisterControllerBase.email');

  @override
  String get email {
    _$emailAtom.context.enforceReadPolicy(_$emailAtom);
    _$emailAtom.reportObserved();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.context.conditionallyRunInAction(() {
      super.email = value;
      _$emailAtom.reportChanged();
    }, _$emailAtom, name: '${_$emailAtom.name}_set');
  }

  final _$passwordAtom = Atom(name: '_RegisterControllerBase.password');

  @override
  String get password {
    _$passwordAtom.context.enforceReadPolicy(_$passwordAtom);
    _$passwordAtom.reportObserved();
    return super.password;
  }

  @override
  set password(String value) {
    _$passwordAtom.context.conditionallyRunInAction(() {
      super.password = value;
      _$passwordAtom.reportChanged();
    }, _$passwordAtom, name: '${_$passwordAtom.name}_set');
  }

  final _$confPasswordAtom = Atom(name: '_RegisterControllerBase.confPassword');

  @override
  String get confPassword {
    _$confPasswordAtom.context.enforceReadPolicy(_$confPasswordAtom);
    _$confPasswordAtom.reportObserved();
    return super.confPassword;
  }

  @override
  set confPassword(String value) {
    _$confPasswordAtom.context.conditionallyRunInAction(() {
      super.confPassword = value;
      _$confPasswordAtom.reportChanged();
    }, _$confPasswordAtom, name: '${_$confPasswordAtom.name}_set');
  }

  final _$_RegisterControllerBaseActionController =
      ActionController(name: '_RegisterControllerBase');

  @override
  void validatePhone() {
    final _$actionInfo =
        _$_RegisterControllerBaseActionController.startAction();
    try {
      return super.validatePhone();
    } finally {
      _$_RegisterControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void changeIsCpf() {
    final _$actionInfo =
        _$_RegisterControllerBaseActionController.startAction();
    try {
      return super.changeIsCpf();
    } finally {
      _$_RegisterControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void changeIsMail() {
    final _$actionInfo =
        _$_RegisterControllerBaseActionController.startAction();
    try {
      return super.changeIsMail();
    } finally {
      _$_RegisterControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setName(String value) {
    final _$actionInfo =
        _$_RegisterControllerBaseActionController.startAction();
    try {
      return super.setName(value);
    } finally {
      _$_RegisterControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setDocument(String value) {
    final _$actionInfo =
        _$_RegisterControllerBaseActionController.startAction();
    try {
      return super.setDocument(value);
    } finally {
      _$_RegisterControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setPhone(String value) {
    final _$actionInfo =
        _$_RegisterControllerBaseActionController.startAction();
    try {
      return super.setPhone(value);
    } finally {
      _$_RegisterControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setEmail(String value) {
    final _$actionInfo =
        _$_RegisterControllerBaseActionController.startAction();
    try {
      return super.setEmail(value);
    } finally {
      _$_RegisterControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setPassword(String value) {
    final _$actionInfo =
        _$_RegisterControllerBaseActionController.startAction();
    try {
      return super.setPassword(value);
    } finally {
      _$_RegisterControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setConfPassword(String value) {
    final _$actionInfo =
        _$_RegisterControllerBaseActionController.startAction();
    try {
      return super.setConfPassword(value);
    } finally {
      _$_RegisterControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic resetData() {
    final _$actionInfo =
        _$_RegisterControllerBaseActionController.startAction();
    try {
      return super.resetData();
    } finally {
      _$_RegisterControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    final string =
        'isCpf: ${isCpf.toString()},isMail: ${isMail.toString()},phoneValidated: ${phoneValidated.toString()},name: ${name.toString()},document: ${document.toString()},phone: ${phone.toString()},email: ${email.toString()},password: ${password.toString()},confPassword: ${confPassword.toString()}';
    return '{$string}';
  }
}
