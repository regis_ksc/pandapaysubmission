import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:pandapaysubmission/app/modules/register/components/logo_and_pop/logo_and_pop.dart';
import 'package:pandapaysubmission/app/modules/register/components/register_email_fields/visibility_true/visibility_true_widget.dart';
import 'package:pandapaysubmission/app/modules/register/components/register_regular_fields/register_regular_fields.dart';
import 'package:pandapaysubmission/app/modules/register/components/submit_button/submit_button_widget.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';
import 'package:pandapaysubmission/app/shared/widgets/custom_button/custom_button.dart';
import 'package:pandapaysubmission/app/shared/widgets/register_with_button/register_with_button.dart';
import 'components/register_button_row/register_button_row.dart';
import 'components/register_email_fields/visibility_false/visibility_false_widget.dart';
import 'register_controller.dart';

class RegisterPage extends StatefulWidget {
  final String title;
  const RegisterPage({Key key, this.title = "Register"}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState
    extends ModularState<RegisterPage, RegisterController> {
  //use 'controller' variable to access controller

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller.resetData();
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardAvoider(
      curve: Curves.easeInCubic,
      duration: Duration(milliseconds: 400),
      child: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            color: white95,
            height: SizeConfig.heightMultiplier * 100,
            child: Form(
              key: controller.formKey,
              autovalidate: true,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    flex: 7,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Expanded(
                          child: LogoAndPop(),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 88,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 6,
                          child: Column(
                            children: <Widget>[],
                          ),
                        ),
                        Expanded(
                          flex: 88,
                          child: Column(
                            children: <Widget>[
                              Expanded(
                                flex: 15,
                                child: Container(
                                  margin: EdgeInsets.only(
                                      bottom: SizeConfig.heightMultiplier *
                                          2.045268),
                                  child: RegisterButtonRow(),
                                ),
                              ),
                              Expanded(
                                flex: 30,
                                child: Container(
                                  padding: EdgeInsets.only(
                                      top: SizeConfig.heightMultiplier *
                                          1.363512),
                                  child: RegisterRegularFields(),
                                ),
                              ),
                              Expanded(
                                flex: 8,
                                child: Container(
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Container(
                                          child: Text(
                                            "Cadastrar com:",
                                            textAlign: TextAlign.center,
                                            style: titleFont.copyWith(
                                              fontSize:
                                                  SizeConfig.heightMultiplier *
                                                      1.908917,
                                              color: Colors.grey[800],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 40,
                                child: Container(
                                  padding: EdgeInsets.only(
                                      top: SizeConfig.heightMultiplier *
                                          1.363512),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Expanded(
                                              child: Column(
                                                children: <Widget>[
                                                  Expanded(
                                                    child:
                                                        Observer(builder: (_) {
                                                      return Container(
                                                        margin:
                                                            EdgeInsets.fromLTRB(
                                                          SizeConfig
                                                                  .heightMultiplier *
                                                              5.45404,
                                                          SizeConfig
                                                                  .heightMultiplier *
                                                              (5.45404 / 8),
                                                          SizeConfig
                                                                  .heightMultiplier *
                                                              5.45404,
                                                          0,
                                                        ),
                                                        child:
                                                            RegisterWithButton(
                                                                onTap: () {
                                                                  controller
                                                                      .changeIsMail();
                                                                },
                                                                textColor: controller
                                                                        .isMail
                                                                    ? white95
                                                                    : Colors.grey[
                                                                        600],
                                                                borderColor: controller
                                                                        .isMail
                                                                    ? blackButtonColor
                                                                    : Colors.grey[
                                                                        600],
                                                                bgColor: controller
                                                                        .isMail
                                                                    ? blackButtonColor
                                                                    : Colors
                                                                        .transparent,
                                                                text:
                                                                    "E-mail/Senha",
                                                                icon: FittedBox(
                                                                    child: Icon(
                                                                  Feather.mail,
                                                                  color: controller
                                                                          .isMail
                                                                      ? Colors
                                                                          .white70
                                                                      : Colors.grey[
                                                                          700],
                                                                ))),
                                                      );
                                                    }),
                                                  ),
                                                  Expanded(
                                                    flex: 3,
                                                    child:
                                                        Observer(builder: (_) {
                                                      return Visibility(
                                                        visible:
                                                            !controller.isMail,
                                                        child:
                                                            VisibilityTrueWidget(),
                                                        replacement:
                                                            VisibilityFalseWidget(),
                                                      );
                                                    }),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 7,
                                child: Row(
                                  children: <Widget>[
                                    Expanded(flex: 1, child: Container()),
                                    Expanded(
                                        flex: 3, child: SubmitButtonWidget()),
                                    Expanded(flex: 1, child: Container()),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 6,
                          child: Container(),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 5,
                    child: Container(),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
