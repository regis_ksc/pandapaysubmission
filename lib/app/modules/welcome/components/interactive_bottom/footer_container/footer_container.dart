import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';
import 'package:pandapaysubmission/app/shared/widgets/custom_button/custom_button.dart';

class FooterContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
          vertical: 2.5906735751295 * SizeConfig.heightMultiplier),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 6,
            child: FittedBox(
              child: Text(
                "Já tem cadastro no PandaPay?",
                style: titleFont.copyWith(
                    color: Colors.grey[700], fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Expanded(
            flex: 4,
            child: Container(
              padding: EdgeInsets.only(
                  left: 1.417635384179 * SizeConfig.widthMultiplier),
              child: CustomButton(
                radius: 0.5670541536 * SizeConfig.widthMultiplier,
                padding: EdgeInsets.symmetric(
                    horizontal: 1.417635384179 * SizeConfig.widthMultiplier,
                    vertical: SizeConfig.heightMultiplier * 0.681756203),
                borderColor: Colors.grey[800],
                onTap: () {
                  Modular.to.pushNamed('/login/');
                },
                color: Colors.transparent,
                text: Text(
                  "Clique Aqui!",
                  style: titleFont.copyWith(
                      color: Colors.grey[700], fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
