import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pandapaysubmission/app/modules/welcome/welcome_controller.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';

class TextContainer extends StatefulWidget {
  @override
  _TextContainerState createState() => _TextContainerState();
}

class _TextContainerState
    extends ModularState<TextContainer, WelcomeController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
          horizontal: 0.56705415367 * SizeConfig.widthMultiplier,
          vertical: 0.27270248 * SizeConfig.heightMultiplier),
      child: Observer(
        builder: (_) {
          return Column(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: 0.68175620398 * SizeConfig.heightMultiplier),
                  child: FittedBox(
                    child: Text(
                      controller.textRow1,
                      textAlign: TextAlign.justify,
                      style: titleFont.copyWith(
                        color: Colors.grey[700],
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: FittedBox(
                  child: Text(
                    controller.textRow2,
                    textAlign: TextAlign.justify,
                    style: titleFont.copyWith(color: Colors.grey[700]),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: 0.68175620398 * SizeConfig.heightMultiplier),
                  child: FittedBox(
                    child: Text(
                      controller.textRow3,
                      textAlign: TextAlign.justify,
                      style: titleFont.copyWith(color: Colors.grey[700]),
                    ),
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
