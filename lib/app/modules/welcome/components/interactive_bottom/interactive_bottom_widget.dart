import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pandapaysubmission/app/modules/welcome/components/interactive_bottom/button_container/button_container.dart';
import 'package:pandapaysubmission/app/modules/welcome/components/interactive_bottom/footer_container/footer_container.dart';
import 'package:pandapaysubmission/app/modules/welcome/components/interactive_bottom/text_container/text_container.dart';
import 'package:pandapaysubmission/app/modules/welcome/welcome_controller.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';

class InteractiveBottomWidget extends StatefulWidget {
  @override
  _InteractiveBottomWidgetState createState() =>
      _InteractiveBottomWidgetState();
}

class _InteractiveBottomWidgetState
    extends ModularState<InteractiveBottomWidget, WelcomeController> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 10,
            child: Container(),
          ),
          Expanded(
            flex: 80,
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 5,
                  child: Container(),
                ),
                Expanded(
                  flex: 55,
                  child: Container(child: Observer(
                    builder: (_) {
                      return Row(
                        children: <Widget>[
                          Expanded(
                            flex: controller.index == 2 ? 8 : 10,
                            child: Container(),
                          ),
                          Expanded(
                            flex: controller.index == 2 ? 84 : 80,
                            child: Column(
                              children: <Widget>[
                                Expanded(
                                  flex: controller.index == 2 ? 48 : 35,
                                  child: Row(
                                    mainAxisAlignment: controller.index == 3
                                        ? MainAxisAlignment.start
                                        : MainAxisAlignment.center,
                                    children: <Widget>[
                                      Expanded(
                                        child: TextContainer(),
                                      )
                                    ],
                                  ),
                                ),
                                Expanded(
                                  flex: controller.index == 2 ? 2 : 15,
                                  child: Container(),
                                ),
                                Expanded(
                                  flex: 50,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      GestureDetector(
                                        onTap: () {
                                          Modular.to.pushNamed('/register/');
                                        },
                                        child: Container(
                                          margin: EdgeInsets.fromLTRB(
                                              0,
                                              1.36351240796 *
                                                  SizeConfig.heightMultiplier,
                                              0,
                                              2.0452686119 *
                                                  SizeConfig.heightMultiplier),
                                          child: ButtonContainer(),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            flex: controller.index == 2 ? 8 : 10,
                            child: Container(),
                          ),
                        ],
                      );
                    },
                  )),
                ),
                Expanded(
                  flex: 13,
                  child: Container(),
                ),
                Expanded(
                  flex: 17,
                  child: FooterContainer(),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 10,
            child: Container(),
          ),
        ],
      ),
    );
  }
}
