import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';
import 'package:pandapaysubmission/app/shared/widgets/custom_button/custom_button.dart';

class ButtonContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        CustomButton(
          radius: 0.567054153 * SizeConfig.widthMultiplier,
          height: 6.8175620398 * SizeConfig.heightMultiplier,
          width: 56.705415367 * SizeConfig.widthMultiplier,
          onTap: () {
            Modular.to.pushNamed('/register/');
          },
          text: Text(
            "Criar minha conta",
            style: buttonContentFont,
          ),
          color: Colors.black,
        ),
        Container(
          height: 0.68175620398 * SizeConfig.heightMultiplier,
        ),
        Text(
          "(leva menos de 2 minutos)",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 1.09080992637 * SizeConfig.heightMultiplier,
              fontFamily: 'OpenSans',
              decoration: TextDecoration.none,
              color: Colors.grey[600]),
        ),
      ],
    );
  }
}
