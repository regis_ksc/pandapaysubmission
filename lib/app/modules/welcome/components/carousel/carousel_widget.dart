import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:pandapaysubmission/app/modules/welcome/welcome_controller.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';

class CarouselWidget extends StatefulWidget {
  @override
  _CarouselWidgetState createState() => _CarouselWidgetState();
}

class _CarouselWidgetState
    extends ModularState<CarouselWidget, WelcomeController> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: <Widget>[
          Observer(
            builder: (_) {
              return Swiper(
                itemCount: 3,
                curve: Curves.easeInOutCubic,
                autoplay: true,
                autoplayDelay: 5000,
                loop: true,
                itemHeight: double.infinity,
                itemWidth: double.infinity,
                index: controller.index,
                onIndexChanged: (int index) {
                  controller.increment();
                  controller.updateText();
                  print(controller.textRow1);
                },
                duration: 700,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage(
                          'assets/images/welcome/pic${index + 1}.png'),
                    )),
                  );
                },
              );
            },
          ),
          Positioned(
            top: 6.81756 * SizeConfig.heightMultiplier,
            child: FittedBox(
              child: Container(
                padding: EdgeInsets.symmetric(
                    horizontal: 7.0881769 * SizeConfig.widthMultiplier),
                height: 10.908099 * SizeConfig.heightMultiplier,
                width: MediaQuery.of(context).size.width,
                child: Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                    fit: BoxFit.fitWidth,
                    image: AssetImage('assets/images/welcome/pandapayh.png'),
                  )),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
