// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'welcome_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$WelcomeController on _WelcomeControllerBase, Store {
  final _$indexAtom = Atom(name: '_WelcomeControllerBase.index');

  @override
  int get index {
    _$indexAtom.context.enforceReadPolicy(_$indexAtom);
    _$indexAtom.reportObserved();
    return super.index;
  }

  @override
  set index(int value) {
    _$indexAtom.context.conditionallyRunInAction(() {
      super.index = value;
      _$indexAtom.reportChanged();
    }, _$indexAtom, name: '${_$indexAtom.name}_set');
  }

  final _$textRow1Atom = Atom(name: '_WelcomeControllerBase.textRow1');

  @override
  String get textRow1 {
    _$textRow1Atom.context.enforceReadPolicy(_$textRow1Atom);
    _$textRow1Atom.reportObserved();
    return super.textRow1;
  }

  @override
  set textRow1(String value) {
    _$textRow1Atom.context.conditionallyRunInAction(() {
      super.textRow1 = value;
      _$textRow1Atom.reportChanged();
    }, _$textRow1Atom, name: '${_$textRow1Atom.name}_set');
  }

  final _$textRow2Atom = Atom(name: '_WelcomeControllerBase.textRow2');

  @override
  String get textRow2 {
    _$textRow2Atom.context.enforceReadPolicy(_$textRow2Atom);
    _$textRow2Atom.reportObserved();
    return super.textRow2;
  }

  @override
  set textRow2(String value) {
    _$textRow2Atom.context.conditionallyRunInAction(() {
      super.textRow2 = value;
      _$textRow2Atom.reportChanged();
    }, _$textRow2Atom, name: '${_$textRow2Atom.name}_set');
  }

  final _$textRow3Atom = Atom(name: '_WelcomeControllerBase.textRow3');

  @override
  String get textRow3 {
    _$textRow3Atom.context.enforceReadPolicy(_$textRow3Atom);
    _$textRow3Atom.reportObserved();
    return super.textRow3;
  }

  @override
  set textRow3(String value) {
    _$textRow3Atom.context.conditionallyRunInAction(() {
      super.textRow3 = value;
      _$textRow3Atom.reportChanged();
    }, _$textRow3Atom, name: '${_$textRow3Atom.name}_set');
  }

  final _$_WelcomeControllerBaseActionController =
      ActionController(name: '_WelcomeControllerBase');

  @override
  dynamic updateText() {
    final _$actionInfo = _$_WelcomeControllerBaseActionController.startAction();
    try {
      return super.updateText();
    } finally {
      _$_WelcomeControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic increment() {
    final _$actionInfo = _$_WelcomeControllerBaseActionController.startAction();
    try {
      return super.increment();
    } finally {
      _$_WelcomeControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    final string =
        'index: ${index.toString()},textRow1: ${textRow1.toString()},textRow2: ${textRow2.toString()},textRow3: ${textRow3.toString()}';
    return '{$string}';
  }
}
