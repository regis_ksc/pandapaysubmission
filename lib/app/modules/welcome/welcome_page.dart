import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pandapaysubmission/app/modules/welcome/components/carousel/carousel_widget.dart';
import 'package:pandapaysubmission/app/modules/welcome/components/interactive_bottom/interactive_bottom_widget.dart';
import 'welcome_controller.dart';

class WelcomePage extends StatefulWidget {
  final String title;
  const WelcomePage({Key key, this.title = "Welcome"}) : super(key: key);

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends ModularState<WelcomePage, WelcomeController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: CarouselWidget(),
        ),
        Expanded(
          child: Container(
            color: Color(0xF5FFFFFF),
            child: InteractiveBottomWidget(),
          ),
        ),
      ],
    );
  }
}
