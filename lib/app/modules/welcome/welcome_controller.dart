import 'package:mobx/mobx.dart';

part 'welcome_controller.g.dart';

class WelcomeController = _WelcomeControllerBase with _$WelcomeController;

abstract class _WelcomeControllerBase with Store {
  @observable
  int index = 0;
  @observable
  String textRow1 = "Cansado de pagar taxas";
  @observable
  String textRow2 = "abusivas para tudo?";
  @observable
  String textRow3 = "O PandaPay é pra você!";

  @action
  updateText() {
    switch (index) {
      case 0:
        textRow1 = "Cansado de pagar taxas";
        textRow2 = "abusivas para tudo?";
        textRow3 = "O PandaPay é pra você!";
        break;
      case 1:
        textRow1 = "Pague de um lugar só:";
        textRow2 = "Contas, boletos, recargas,";
        textRow3 = "e muito mais.";
        break;
      case 2:
        textRow1 = "Transferências instantâneas";
        textRow2 = "e ilimitadas pra você, sem taxas.";
        textRow3 = "Com a segurança do Blockchain.";
        break;
    }
  }

  @action
  increment() {
    if (index == 2)
      index = 0;
    else
      index++;
    print(index);
  }
}
