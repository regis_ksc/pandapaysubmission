import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';
import 'package:pandapaysubmission/app/shared/widgets/custom_button/custom_button.dart';

class LoginButtonAndForgotPass extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
          color: Colors.grey[800],
          child: CustomButton(
            radius: SizeConfig.widthMultiplier * 7.0881769,
            borderColor: Colors.transparent,
            color: Colors.grey[800],
            height: SizeConfig.heightMultiplier * 5.454049,
            onTap: () {
              Modular.to.pushReplacementNamed('/home/');
            },
            text: Text(
              "Entrar",
              style: buttonContentFont.copyWith(),
            ),
            width: double.infinity,
          ),
        ),
        SizedBox(
          height: SizeConfig.heightMultiplier * 0.681756,
        ),
        FittedBox(
          child: Container(
            margin: EdgeInsets.symmetric(
                horizontal: SizeConfig.widthMultiplier * 17.01162),
            child: Text(
              "Esqueceu sua senha do PandaPay?",
              style: titleFont.copyWith(color: Colors.blueGrey[800]),
            ),
          ),
        )
      ],
    );
  }
}
