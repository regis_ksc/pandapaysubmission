import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:pandapaysubmission/app/modules/login/login_button_and_forgot_pass/login_button_and_forgot_pass.dart';
import 'package:pandapaysubmission/app/modules/login/login_fb_google/login_fb_google.dart';
import 'package:pandapaysubmission/app/modules/login/login_fields/login_fields.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'login_controller.dart';

class LoginPage extends StatefulWidget {
  final String title;
  const LoginPage({Key key, this.title = "Login"}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends ModularState<LoginPage, LoginController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        height: SizeConfig.heightMultiplier * 100,
        color: white95,
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 6,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 20,
                    child: Container(
                      child: Center(
                        child: GestureDetector(
                          onTap: () {
                            Modular.to.pop();
                          },
                          child: Icon(
                            Ionicons.md_arrow_round_back,
                            size: 8.505812 * SizeConfig.widthMultiplier,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 80,
                    child: Container(),
                  )
                ],
              ),
            ),
            Expanded(
              flex: 94,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 10,
                    child: Column(
                      children: <Widget>[],
                    ),
                  ),
                  Expanded(
                    flex: 80,
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          flex: 10,
                          child: Row(
                            children: <Widget>[],
                          ),
                        ),
                        Expanded(
                          flex: 80,
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  child: Column(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 20,
                                        child: Container(
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                                image: AssetImage(
                                                    'assets/images/register/pandapayhb.png'),
                                                fit: BoxFit.contain),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 30,
                                        child: LoginFields(),
                                      ),
                                      Expanded(
                                          flex: 50,
                                          child: Row(
                                            children: <Widget>[
                                              Expanded(
                                                  flex: 10, child: Container()),
                                              Expanded(
                                                  flex: 80,
                                                  child: Column(
                                                    children: <Widget>[
                                                      Expanded(
                                                        flex: 20,
                                                        child:
                                                            LoginButtonAndForgotPass(),
                                                      ),
                                                      Expanded(
                                                          flex: 4,
                                                          child: Container()),
                                                      Expanded(
                                                        flex: 26,
                                                        child: LoginFbGoogle(),
                                                      ),
                                                    ],
                                                  )),
                                              Expanded(
                                                  flex: 10, child: Container()),
                                            ],
                                          ))
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 10,
                          child: Row(
                            children: <Widget>[],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 10,
                    child: Column(
                      children: <Widget>[],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
