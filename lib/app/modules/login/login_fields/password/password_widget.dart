import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';

class PasswordWidget extends StatelessWidget {
  TextEditingController passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
          flex: 30,
          child: Container(
            child: Observer(builder: (_) {
              return Text(
                "Senha",
                style: titleFont.copyWith(
                    fontSize: SizeConfig.heightMultiplier * 1.908917,
                    color: Colors.grey[800]),
              );
            }),
          ),
        ),
        Expanded(
          flex: 70,
          child: Material(
            color: Colors.transparent,
            child: Container(
              padding: EdgeInsets.symmetric(
                vertical: 10,
              ),
              child: Observer(builder: (_) {
                return TextFormField(
                  style: registerFormFieldFont.copyWith(
                      letterSpacing: SizeConfig.widthMultiplier * 0.567054),
                  cursorWidth: 0.1,
                  controller: passwordController,
                  obscureText: true,
                  onChanged: (String text) {},
                  decoration: InputDecoration(
                    hintText: "Senha cadastrada",
                    hasFloatingPlaceholder: true,
                    hintStyle: TextStyle(
                        color: Colors.grey[400],
                        fontFamily: 'OpenSans',
                        fontSize: SizeConfig.heightMultiplier * 1.63621,
                        letterSpacing: 0),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          width: 0.283527 * SizeConfig.widthMultiplier,
                          color: Colors.grey[500]),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          width: 1.5 * 0.283527 * SizeConfig.widthMultiplier,
                          color: Colors.blueGrey[800]),
                    ),
                    errorBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          width: 0.283527 * SizeConfig.widthMultiplier,
                          color: Colors.blueGrey[500]),
                    ),
                    contentPadding: EdgeInsets.symmetric(
                      vertical: SizeConfig.heightMultiplier * 2.045268,
                    ),
                  ),
                );
              }),
            ),
          ),
        ),
      ],
    );
  }
}
