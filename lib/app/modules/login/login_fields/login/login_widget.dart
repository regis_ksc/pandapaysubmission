import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';

class LoginWidget extends StatelessWidget {
  TextEditingController loginController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
          flex: 30,
          child: Container(
            child: Observer(builder: (_) {
              return Text(
                "Login",
                style: titleFont.copyWith(
                    fontSize: SizeConfig.heightMultiplier * 1.908917,
                    color: Colors.grey[800]),
              );
            }),
          ),
        ),
        Expanded(
          flex: 70,
          child: Material(
            color: Colors.transparent,
            child: Container(
              padding: EdgeInsets.symmetric(
                  vertical: SizeConfig.heightMultiplier * 1.363512),
              child: Observer(builder: (_) {
                return TextFormField(
                  style: registerFormFieldFont,
                  textCapitalization: TextCapitalization.words,
                  cursorWidth: 0.1,
                  controller: loginController,
                  onChanged: (String text) {},
                  decoration: InputDecoration(
                    hintText: "E-mail ou CPF/CNPJ",
                    hasFloatingPlaceholder: true,
                    hintStyle: TextStyle(
                        color: Colors.grey[400],
                        fontFamily: 'OpenSans',
                        fontSize: 1.636214 * SizeConfig.heightMultiplier),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          width: SizeConfig.widthMultiplier * 0.283527,
                          color: Colors.grey[500]),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          width: 1.5 * SizeConfig.widthMultiplier * 0.283527,
                          color: Colors.blueGrey[800]),
                    ),
                    errorBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          width: SizeConfig.widthMultiplier * 0.283527,
                          color: Colors.blueGrey[500]),
                    ),
                    contentPadding: EdgeInsets.symmetric(
                      vertical: SizeConfig.heightMultiplier * 2.045268,
                    ),
                  ),
                );
              }),
            ),
          ),
        ),
      ],
    );
  }
}
