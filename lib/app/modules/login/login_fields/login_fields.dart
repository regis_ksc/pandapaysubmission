import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:pandapaysubmission/app/modules/login/login_fields/login/login_widget.dart';
import 'package:pandapaysubmission/app/modules/login/login_fields/password/password_widget.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';

class LoginFields extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(child: LoginWidget()),
        Expanded(child: PasswordWidget()),
      ],
    );
  }
}
