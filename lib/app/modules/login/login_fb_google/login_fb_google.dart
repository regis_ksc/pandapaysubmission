import 'package:flutter/material.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';
import 'package:pandapaysubmission/app/shared/widgets/register_with_button/register_with_button.dart';

class LoginFbGoogle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
            flex: 20,
            child: Text(
              "Acessar com:",
              style: titleFont.copyWith(
                  color: Colors.blueGrey[800],
                  fontSize: SizeConfig.heightMultiplier * 1.90891),
            )),
        Expanded(
          flex: 39,
          child: Card(
            color: facebookBlue,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(
                    SizeConfig.widthMultiplier * 7.0881769)),
            child: RegisterWithButton(
              textColor: Colors.white70,
              bgColor: facebookBlue,
              borderColor: facebookBlue,
              text: "Facebook",
              icon: Container(
                margin: EdgeInsets.symmetric(
                    vertical: SizeConfig.heightMultiplier * 0.272702,
                    horizontal: SizeConfig.widthMultiplier * 0.567054),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.fitHeight,
                    image: AssetImage(
                      'assets/images/register/facebook.png',
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        Expanded(flex: 2, child: Container()),
        Expanded(
          flex: 39,
          child: Card(
            color: Colors.white70,
            elevation: 1,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25),
                  border: Border.all(
                      color: Colors.black12,
                      width: SizeConfig.widthMultiplier * 0.283527)),
              child: RegisterWithButton(
                textColor: Colors.black87,
                borderColor: Colors.transparent,
                bgColor: Colors.white70,
                text: "Google",
                icon: Container(
                  margin: EdgeInsets.symmetric(
                      vertical: SizeConfig.heightMultiplier * 0.272702,
                      horizontal: SizeConfig.widthMultiplier * 0.567054),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.fitHeight,
                      image: AssetImage(
                        'assets/images/register/google.png',
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
