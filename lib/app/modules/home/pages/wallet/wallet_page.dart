import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pandapaysubmission/app/modules/home/pages/wallet/components/wallet_balance/balance_button/balance_button.dart';
import 'package:pandapaysubmission/app/modules/home/pages/wallet/components/wallet_balance/wallet_balance.dart';
import 'package:pandapaysubmission/app/modules/home/pages/wallet/components/wallet_cards/wallet_cards.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';
import 'components/wallet_footer/wallet_footer.dart';
import 'components/wallet_header/wallet_header.dart';
import 'wallet_controller.dart';

class WalletPage extends StatefulWidget {
  final String title;
  const WalletPage({Key key, this.title = "Wallet"}) : super(key: key);

  @override
  _WalletPageState createState() => _WalletPageState();
}

class _WalletPageState extends ModularState<WalletPage, WalletController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.heightMultiplier * 100,
      color: white95,
      child: Column(
        children: <Widget>[
          Expanded(flex: 15, child: WalletHeader()),
          Expanded(flex: 17, child: WalletBalance()),
          Expanded(flex: 51, child: WalletCards()),
          Expanded(
              flex: 17,
              child: Container(
                color: Colors.black87,
                margin: EdgeInsets.only(
                    bottom: 0.040905372238 * SizeConfig.heightMultiplier),
                child: WalletFooter(),
              )),
        ],
      ),
    );
  }
}
