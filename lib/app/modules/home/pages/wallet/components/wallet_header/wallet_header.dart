import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';

class WalletHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: SizeConfig.heightMultiplier * 2.72702),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
              flex: 15,
              child: Container(
                padding: EdgeInsets.only(
                    top: SizeConfig.heightMultiplier * 0.681756),
                alignment: Alignment.topCenter,
                child: GestureDetector(
                  child: Icon(Octicons.three_bars,
                      size: SizeConfig.widthMultiplier * 8.50581),
                ),
              )),
          Expanded(
            flex: 70,
            child: Container(
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 8,
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: Container(
                            alignment: Alignment.topLeft,
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                    flex: 3,
                                    child: Container(
                                      margin: EdgeInsets.symmetric(
                                          vertical:
                                              SizeConfig.heightMultiplier *
                                                  1.090809,
                                          horizontal:
                                              SizeConfig.widthMultiplier *
                                                  2.268216),
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  'assets/images/home/panda_icon.png'),
                                              fit: BoxFit.contain)),
                                    )),
                                Expanded(
                                    flex: 7,
                                    child: Container(
                                      child: RichText(
                                        text: TextSpan(
                                            text: "Olá ",
                                            style: varelaFont.copyWith(
                                                color: Colors.blueGrey[800],
                                                fontSize: SizeConfig
                                                        .heightMultiplier *
                                                    2.727024),
                                            children: [
                                              TextSpan(
                                                text: "Nome",
                                                style: varelaFont.copyWith(
                                                    fontSize: SizeConfig
                                                            .heightMultiplier *
                                                        2.727024,
                                                    color: Colors.blueGrey[800],
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              TextSpan(
                                                text: "!",
                                                style: varelaFont.copyWith(
                                                  color: Colors.blueGrey[800],
                                                  fontSize: SizeConfig
                                                          .heightMultiplier *
                                                      2.727024,
                                                ),
                                              ),
                                            ]),
                                        textAlign: TextAlign.start,
                                      ),
                                    )),
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
              flex: 15,
              child: Container(
                padding: EdgeInsets.only(
                    top: SizeConfig.heightMultiplier * 1.363512,
                    right: SizeConfig.widthMultiplier * 1.41763),
                alignment: Alignment.topCenter,
                child: GestureDetector(
                  child: Icon(
                    FontAwesome5.bell,
                    size: SizeConfig.widthMultiplier * 7.3717,
                  ),
                ),
              )),
        ],
      ),
    );
  }
}
