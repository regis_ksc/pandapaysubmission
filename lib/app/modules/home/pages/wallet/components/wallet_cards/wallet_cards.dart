import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:pandapaysubmission/app/modules/home/pages/wallet/components/wallet_cards/rounded_card/rounded_card_widget.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';

class WalletCards extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
          vertical: SizeConfig.heightMultiplier * 6.135805,
          horizontal: SizeConfig.widthMultiplier * 2.83527),
      child: Container(
        child: Row(
          children: <Widget>[
            Expanded(
                child: Column(
              children: <Widget>[
                Expanded(
                    child: RoundedCardWidget(
                  color: Colors.grey[700],
                  icon: MaterialCommunityIcons.credit_card,
                  label: "Peça já seu cartão",
                )),
              ],
            )),
            Expanded(
                child: Column(
              children: <Widget>[
                Expanded(
                    child: RoundedCardWidget(
                  color: Colors.blueAccent[700],
                  icon: MaterialCommunityIcons.cellphone,
                  label: "Carregar Celular",
                )),
                Expanded(
                    child: RoundedCardWidget(
                  color: Colors.redAccent[700],
                  icon: MaterialCommunityIcons.bus_side,
                  label: "Carregar \nVale-Transporte",
                )),
              ],
            )),
            Expanded(
                child: Column(
              children: <Widget>[
                Expanded(
                    child: RoundedCardWidget(
                  color: Colors.lime[700],
                  icon: MaterialCommunityIcons.ticket_percent,
                  label: "Vouchers e Crédito",
                )),
                Expanded(
                    child: RoundedCardWidget(
                  color: Colors.lightGreenAccent[700],
                  icon: MaterialIcons.help_outline,
                  label: "Fale Conosco",
                )),
              ],
            )),
          ],
        ),
      ),
    );
  }
}
