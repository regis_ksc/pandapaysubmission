import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';

class RoundedCardWidget extends StatelessWidget {
  final IconData icon;
  final String label;
  final Color color;

  const RoundedCardWidget({Key key, this.icon, this.label, this.color})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
          vertical: SizeConfig.heightMultiplier * 0.95445,
          horizontal: SizeConfig.widthMultiplier * 1.98468),
      decoration: BoxDecoration(
        color: color,
        borderRadius:
            BorderRadius.circular(SizeConfig.widthMultiplier * 4.2529061),
      ),
      child: FittedBox(
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.widthMultiplier * 4.252906,
              vertical: SizeConfig.heightMultiplier * 2.727024),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                width: SizeConfig.widthMultiplier * 17.011624,
                height: SizeConfig.heightMultiplier * 8.18107,
                child: FittedBox(
                  fit: BoxFit.contain,
                  alignment: Alignment.topCenter,
                  child: Icon(
                    icon,
                    color: white95,
                  ),
                ),
              ),
              SizedBox(
                height: SizeConfig.heightMultiplier * 1.36351,
              ),
              Container(
                alignment: Alignment.bottomCenter,
                child: Text(
                  label,
                  textAlign: TextAlign.center,
                  style: varelaFont.copyWith(
                      color: white95,
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.heightMultiplier * 2.181619),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
