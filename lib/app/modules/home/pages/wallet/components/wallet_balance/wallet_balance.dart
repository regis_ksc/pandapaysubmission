import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';

import 'balance_button/balance_button.dart';

class WalletBalance extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.black87, borderRadius: BorderRadius.circular(1)),
      child: Row(
        children: <Widget>[
          Expanded(flex: 5, child: Container()),
          Expanded(
              flex: 90,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 25,
                    child: Container(
                      margin: EdgeInsets.symmetric(
                          vertical: SizeConfig.heightMultiplier * 2.045268,
                          horizontal:
                              SizeConfig.heightMultiplier * 2.045268 * (2 / 3)),
                      child: BalanceButton(
                        label: "Adicionar",
                        bgColor: lightGreenOpaque,
                        icon: Icon(
                          Entypo.plus,
                          color: white95,
                        ),
                        hPadding: SizeConfig.widthMultiplier * 1.9846895,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 50,
                    child: Container(
                      margin: EdgeInsets.symmetric(
                          vertical: SizeConfig.heightMultiplier * 2.045268,
                          horizontal:
                              SizeConfig.heightMultiplier * 2.045268 * (2 / 3)),
                      child: Column(
                        children: <Widget>[
                          Expanded(
                              flex: 30,
                              child: Container(
                                child: FittedBox(
                                  child: Text(
                                    "Saldo Total",
                                    style: varelaFont.copyWith(
                                        color: white95,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              )),
                          Expanded(
                              flex: 70,
                              child: Container(
                                child: FittedBox(
                                  child: RichText(
                                    text: TextSpan(
                                        text: "R\$ 0,",
                                        style: varelaFont.copyWith(
                                          fontSize:
                                              SizeConfig.heightMultiplier *
                                                  4.63594218,
                                          color: white95,
                                        ),
                                        children: [
                                          TextSpan(
                                              text: "00",
                                              style: varelaFont.copyWith(
                                                  fontSize: SizeConfig
                                                          .heightMultiplier *
                                                      2.7270248))
                                        ]),
                                  ),
                                ),
                              )),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 25,
                    child: Container(
                      margin: EdgeInsets.symmetric(
                          vertical: SizeConfig.heightMultiplier * 2.045268,
                          horizontal:
                              SizeConfig.heightMultiplier * 2.045268 * (2 / 3)),
                      child: BalanceButton(
                        icon: Icon(
                          Entypo.minus,
                          color: white95,
                        ),
                        label: "Retirar",
                        bgColor: lightRedOpaque,
                        hPadding: SizeConfig.widthMultiplier * 2.268216,
                        vPadding: SizeConfig.heightMultiplier * 0.1363512,
                      ),
                    ),
                  ),
                ],
              )),
          Expanded(flex: 5, child: Container()),
        ],
      ),
    );
  }
}
