import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';

class BalanceButton extends StatelessWidget {
  final String label;
  final Color bgColor;
  final Function onTap;
  final Icon icon;
  final double hPadding;
  final double vPadding;

  const BalanceButton(
      {Key key,
      this.label,
      this.bgColor,
      this.onTap,
      this.icon,
      this.hPadding,
      this.vPadding = 0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
          vertical: SizeConfig.heightMultiplier * 0.545404),
      child: Column(
        children: <Widget>[
          Expanded(
              flex: 80,
              child: Container(
                padding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.widthMultiplier * 1.41763,
                    vertical: SizeConfig.heightMultiplier * 0.681756),
                margin: EdgeInsets.symmetric(
                    vertical: SizeConfig.heightMultiplier * 0.2727024,
                    horizontal: SizeConfig.widthMultiplier * 0.56705),
                decoration: BoxDecoration(
                    color: bgColor,
                    borderRadius: BorderRadius.circular(
                        SizeConfig.widthMultiplier * 2.83527)),
                child: FittedBox(
                  child: icon,
                ),
              )),
          Expanded(
              flex: 20,
              child: Container(
                margin: EdgeInsets.symmetric(
                    horizontal: hPadding, vertical: vPadding),
                child: FittedBox(
                  child: Text(
                    label,
                    textAlign: TextAlign.center,
                    style: varelaFont.copyWith(color: white95),
                  ),
                ),
              )),
        ],
      ),
    );
  }
}
