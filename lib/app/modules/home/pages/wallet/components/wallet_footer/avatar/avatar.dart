import 'package:flutter/material.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';

class Avatar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: SizeConfig.widthMultiplier * 4.252906),
      child: Container(
        margin: EdgeInsets.symmetric(
            vertical: SizeConfig.heightMultiplier * 1.363512,
            horizontal: SizeConfig.widthMultiplier * 2.83527),
        padding: EdgeInsets.symmetric(
            vertical: SizeConfig.heightMultiplier * 1.363512 / 2,
            horizontal: SizeConfig.widthMultiplier * 2.83527 / 2),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.white54),
          borderRadius: BorderRadius.circular(30000),
          gradient: LinearGradient(
              colors: [Colors.amber[400], Colors.amber[700]],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight),
        ),
        child: Container(
          margin: EdgeInsets.symmetric(
              vertical: SizeConfig.heightMultiplier * 1.363512 / 2,
              horizontal: SizeConfig.widthMultiplier * 2.83527 / 2),
          decoration: BoxDecoration(
            image: DecorationImage(
                alignment: Alignment.center,
                image: AssetImage('assets/images/home/chat.png'),
                fit: BoxFit.cover),
          ),
        ),
      ),
    );
  }
}
