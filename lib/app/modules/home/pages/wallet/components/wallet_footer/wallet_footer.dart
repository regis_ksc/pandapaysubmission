import 'package:flutter/material.dart';
import 'package:pandapaysubmission/app/modules/home/pages/wallet/components/wallet_footer/avatar/avatar.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';

class WalletFooter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          flex: 35,
          child: Avatar(),
        ),
        Expanded(
          flex: 65,
          child: Center(
              child: Container(
            padding: EdgeInsets.symmetric(
                vertical: SizeConfig.heightMultiplier * 1.363512,
                horizontal: SizeConfig.widthMultiplier * 2.83527),
            child: RichText(
              text: TextSpan(
                text: "Traga seus amigos e transfira ilimitadamente!\n\n",
                style: varelaFont.copyWith(
                    color: white95,
                    fontWeight: FontWeight.w900,
                    fontSize: SizeConfig.heightMultiplier * 1.636214),
                children: [
                  TextSpan(
                      text:
                          "Transferências entre usuários PandaPay são grátis e não dependem do horário bancário. Quantas vezes quiser e quando quiser.",
                      style: varelaFont.copyWith(
                          fontWeight: FontWeight.w500,
                          fontSize: SizeConfig.heightMultiplier * 1.363512))
                ],
              ),
            ),
          )),
        ),
      ],
    );
  }
}
