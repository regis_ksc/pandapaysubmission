import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pandapaysubmission/app/modules/home/pages/wallet/wallet_page.dart';
import 'package:pandapaysubmission/app/shared/utils/constants.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'home_controller.dart';

class HomePage extends StatefulWidget {
  final String title;
  const HomePage({Key key, this.title = "Home"}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage, HomeController> {
  //use 'controller' variable to access controller

  PersistentTabController _tabController =
      PersistentTabController(initialIndex: 0);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        child: PersistentTabView(
          showElevation: true,
          bottomPadding: SizeConfig.heightMultiplier * 1.363512,
          controller: _tabController,
          navBarStyle: NavBarStyle.style6,
          backgroundColor: Colors.black87,
          iconSize: 20,
          items: [
            PersistentBottomNavBarItem(
                icon: Icon(Entypo.wallet),
                titleFontSize: 12,
                title: "Carteira",
                activeColor: white95,
                inactiveColor: Colors.grey[700]),
            PersistentBottomNavBarItem(
                icon: Icon(FontAwesome5.money_bill_alt),
                titleFontSize: 12,
                title: "Pagar",
                activeColor: white95,
                inactiveColor: Colors.grey[700]),
            PersistentBottomNavBarItem(
                icon: Icon(AntDesign.swap),
                titleFontSize: 12,
                title: "Transferir",
                activeColor: white95,
                inactiveColor: Colors.grey[700]),
            PersistentBottomNavBarItem(
                icon: Icon(FontAwesome.cubes),
                titleFontSize: 12,
                title: "BlockChain",
                activeColor: white95,
                inactiveColor: Colors.grey[700]),
            PersistentBottomNavBarItem(
                icon: Icon(FontAwesome.gear),
                titleFontSize: 12,
                title: "Ajustes",
                activeColor: white95,
                inactiveColor: Colors.grey[700]),
          ],
          screens: <Widget>[
            WalletPage(),
            SingleChildScrollView(
              child: Container(
                height: SizeConfig.heightMultiplier * 100,
                width: SizeConfig.widthMultiplier * 100,
                color: Colors.orange,
                child: Container(
                  height: SizeConfig.heightMultiplier * 70,
                  width: SizeConfig.widthMultiplier * 70,
                ),
              ),
            ),
            Container(
              color: Colors.red,
            ),
            Container(
              color: Colors.grey,
            ),
            Container(
              color: Colors.yellow,
            ),
          ],
          onItemSelected: (index) {
            setState(() {
              print(index);
            });
          },
        ),
        onWillPop: () {},
      ),
    );
  }
}
