import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pandapaysubmission/app/shared/utils/size_config.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    FocusScopeNode currentFocus = FocusScope.of(context);
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(builder: (context, orientation) {
          SizeConfig().init(constraints, orientation);
          return GestureDetector(
            excludeFromSemantics: true,
            onTap: () {
              !currentFocus.hasPrimaryFocus ? currentFocus.unfocus() : null;
            },
            child: MaterialApp(
              debugShowCheckedModeBanner: false,
              navigatorKey: Modular.navigatorKey,
              theme: ThemeData().copyWith(),
              title: 'Panda Pay by Regis Kian',
              initialRoute: '/',
              onGenerateRoute: Modular.generateRoute,
            ),
          );
        });
      },
    );
  }
}
